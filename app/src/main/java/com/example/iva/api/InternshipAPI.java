package com.example.iva.api;

import com.example.iva.data.model.Internship;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Interface for internship rest api call declarations
 */
public interface InternshipAPI {
    @GET("internships")
    Call<List<Internship>> getAllInternship();

    @Headers("Content-Type: application/json")
    @POST("internships")
    Call<Void> newInternship(@Body Internship body);
}
