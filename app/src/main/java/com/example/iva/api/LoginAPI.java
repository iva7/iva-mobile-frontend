package com.example.iva.api;

import com.example.iva.data.model.LoginRequest;
import com.example.iva.data.model.Token;
import com.example.iva.data.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface LoginAPI {
    @POST("users/login")
    Call<Token> loginUser(@Body LoginRequest body);

    @GET("account")
    Call<User> getUser(@Header("Authorization") String token);
}
