package com.example.iva.api;

import com.example.iva.data.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RegistrationAPI {
    @Headers("Content-Type: application/json")
    @POST("users/register")
    Call<Void> register(@Body User body);
}
