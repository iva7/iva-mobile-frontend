package com.example.iva.ui.loginAndRegistration;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.iva.R;
import com.example.iva.data.LoginRepository;
import com.example.iva.data.model.LoggedInUser;
import com.example.iva.data.model.Result;
import com.example.iva.ui.utils.Constants;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();
    private LoginRepository loginRepository;

    private @Constants.UserType String selectedUserType;

    public LoginViewModel(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    /**
     * Calls the repository's login method
     *
     * @param username
     * @param password
     */
    public void login(String username, String password) {
        loginRepository.login(username, password, this);
    }

    /**
     * Gets back the result of the login from the repository
     *
     * @param result
     */
    public void getRepoLoginResult(Result<LoggedInUser> result) {
        if (result instanceof Result.Success) {
            LoggedInUser data = ((Result.Success<LoggedInUser>) result).getData();
            loginResult.setValue(new LoginResult(new LoggedInUserView(data.getDisplayName())));
        } else {
            loginResult.setValue(new LoginResult(R.string.login_failed));
        }
    }


    public void newUserTypeChanged(@Constants.UserType  String userType){
        selectedUserType = userType;
    }

    /**
     * Checks the form's validity
     *
     * @param username typed in username
     * @param password typed in password
     */
    public void loginDataChanged(String username, String password) {
        if (username == null) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_username, null));
        } else if (password == null || password.trim().length() < 4) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_password));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }

    public String getSelectedUserType() {
        return selectedUserType;
    }

}
