package com.example.iva.ui.creator;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.iva.R;
import com.example.iva.data.InternshipRepository;
import com.example.iva.data.model.Internship;
import com.example.iva.ui.utils.Constants;

import java.util.Calendar;

public class CreatorViewModel extends ViewModel {

    private MutableLiveData<InternshipFormState> newInternshipFormState = new MutableLiveData<>();

    private InternshipRepository internshipRepository;

    private MutableLiveData<InternshipResult> newInternshipResult = new MutableLiveData<>();

    private boolean payed = false;
    private Internship internship = new Internship();
    private boolean[] validMandatoryFields = new boolean[]{false, false, false, false, false};


    public CreatorViewModel(InternshipRepository InternshipRepository) {
        this.internshipRepository = InternshipRepository;
    }

    LiveData<InternshipFormState> getNewInternshipFormState() {
        return newInternshipFormState;
    }

    LiveData<InternshipResult> getNewInternshipResult() {
        return newInternshipResult;
    }

    /**
     * Calls the repository's newInternship method
     */
    public void newInternship() {

        if (!payed) {
            setSalary(0);
        }
        internshipRepository.newInternship(internship,  this);
    }

    /**
     * Gets back the result of the new internship creation from the repository
     *
     * @param result  true at successful new internship post and false otherwise
     * @param code    error code
     * @param message meaningful error message
     */
    public void getRepoNewInternshipResult(boolean result, int code, String message) {
        if (result) {
            newInternshipResult.setValue(new InternshipResult(true));
        } else {
            newInternshipResult.setValue(new InternshipResult(code, message));
        }
    }


    public void setPayed(boolean checked) {
        this.payed = checked;
    }


    public void setSalary(double price) {
        internship.setSalary(price);
    }

    public void setEducation(@Constants.Level String education) {
        if (education != null) {
            internship.setEducation(education);
        }
    }

    public void setDescription(String description) {
        if (description != "") internship.setDescription(description);
    }

    public void newInternshipRoleChanged(String role) {
        if (role.isEmpty()) {
            validMandatoryFields[0] = false;
            newInternshipFormState.setValue(new InternshipFormState(R.string.empty_fields, null));
        } else {
            internship.setRole(role);
            validMandatoryFields[0] = true;
            validateAll();
        }
    }

    public void newInternshipCompanyChanged(String company) {
        if (company.isEmpty()) {
            validMandatoryFields[1] = false;
            newInternshipFormState.setValue(new InternshipFormState(R.string.empty_fields, null));
        } else {
            internship.setCompany(company);
            validMandatoryFields[1] = true;
            validateAll();
        }
    }

    public void newInternshipLocationChanged(String loc) {
        if (loc.isEmpty()) {
            validMandatoryFields[2] = false;
            newInternshipFormState.setValue(new InternshipFormState(R.string.empty_fields, null));
        } else {
            internship.setLocation(loc);
            validMandatoryFields[2] = true;
            validateAll();
        }
    }

    public void newInternshipDurationChanged(String duration) {
        if (duration.isEmpty()) {
            validMandatoryFields[3] = false;
            newInternshipFormState.setValue(new InternshipFormState(R.string.empty_fields, null));
        } else {
            try {
                internship.setDuration(Integer.parseInt(duration));
                validMandatoryFields[3] = true;
                validateAll();
            } catch (Exception e) {
                validMandatoryFields[3] = false;
                newInternshipFormState.setValue(new InternshipFormState(R.string.invalid_data, null));
            }
        }
    }

    /**
     * Checks the form's dates validity
     */
    public void newInternshipDateChanged(String startDate){
        if (!areDateValid(startDate)) {
            validMandatoryFields[4] = false;
            newInternshipFormState.setValue(new InternshipFormState(R.string.invalid_date, null));
        } else {
            validMandatoryFields[4] = true;
            validateAll();

        }
    }

    /**
     * Checks if every obligatory field is filled and valid
     */
    private void validateAll(){
        for (boolean b : validMandatoryFields) if(!b) {
            newInternshipFormState.setValue(new InternshipFormState(null, R.string.empty_fields));
            return;
        }
        newInternshipFormState.setValue(new InternshipFormState(true));
    }


    /**
     * A placeholder time interval validation check
     *
     * @param startDate
     * @return true if the start date is in the future, false otherwise
     */
    private boolean areDateValid(String startDate) {
        if (startDate == null || startDate.isEmpty()) {
            return false;
        }
        String[] time = startDate.split("\\s+|:\\s*|-\\s*");
        Calendar t1 = Calendar.getInstance();
        t1.set(Integer.parseInt(time[0]), Integer.parseInt(time[1])- 1, Integer.parseInt(time[2]),
                0, 0);
        internship.setStartDate(t1.getTimeInMillis() / 1000);
        return t1.after(Calendar.getInstance());
    }
}