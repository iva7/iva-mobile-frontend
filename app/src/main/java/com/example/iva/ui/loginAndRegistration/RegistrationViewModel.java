package com.example.iva.ui.loginAndRegistration;

import android.util.Patterns;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.iva.R;
import com.example.iva.data.RegistrationRepository;


/**
 * View model for user registration
 */
public class RegistrationViewModel extends ViewModel {
    private MutableLiveData<RegistrationFormState> registrationFormState = new MutableLiveData<>();
    private RegistrationRepository registrationRepository;
    private MutableLiveData<RegistrationResult> registrationResult = new MutableLiveData<>();

    public RegistrationViewModel(RegistrationRepository registrationRepository) {
        this.registrationRepository = registrationRepository;
    }

    LiveData<RegistrationFormState> getRegistrationFormState() {
        return registrationFormState;
    }

    LiveData<RegistrationResult> getRegistrationResult() {
        return registrationResult;
    }

    /**
     * Calls the repository's registration method
     *
     * @param username
     * @param fName    first name
     * @param lName    last name
     * @param password
     * @param email
     * @param role user type
     */
    public void registration(String username, String fName, String lName, String password,
                             String email, String role) {
        registrationRepository.registration(username, fName, lName, password, email, role, this);

    }

    /**
     * Gets back the result of the login from the repository
     *
     * @param result  true at successful registration and false otherwise
     * @param code    error code
     * @param message meaningful error message
     */
    public void getRepoRegistrationResult(boolean result, int code, String message) {
        if (result) {
            registrationResult.setValue(new RegistrationResult(true));
        } else {
            registrationResult.setValue(new RegistrationResult(code, message));
        }
    }

    /**
     * Checks the form's validity
     *
     * @param username
     * @param fName     first name
     * @param lName     last name
     * @param email
     * @param password1
     * @param password2
     * @param role user type
     */
    public void registrationDataChanged(String username, String fName, String lName, String email,
                                        String password1, String password2, String role) {
        if (!everyDataIsFilled(username, fName, lName, email, role, password1, password2)) {
            registrationFormState.setValue(new RegistrationFormState(null, null, R.string.empty_fields));
        } else if (!isEmailValid(email)) {
            registrationFormState.setValue(new RegistrationFormState(R.string.invalid_email, null, null));
        } else if (!isPasswordValid(password1, password2)) {
            registrationFormState.setValue(new RegistrationFormState(null, R.string.invalid_passwords, null));
        } else {
            registrationFormState.setValue(new RegistrationFormState(true));
        }
    }

    /**
     * A placeholder email validation check
     *
     * @param email
     * @return true if the the email is valid, false otherwise
     */
    private boolean isEmailValid(String email) {
        if (email == null) {
            return false;
        }
        if (email.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches();
        } else {
            return email.trim().isEmpty();
        }
    }

    /**
     * A placeholder password validation check
     *
     * @param password1
     * @param password2
     * @return true if the passwords are valid and match, false otherwise
     */
    private boolean isPasswordValid(String password1, String password2) {
        return password1 != null && password1.trim().length() > 3 && password1.contentEquals(password2);
    }

    /**
     * Checks if every field is filled
     *
     * @param username
     * @param fName
     * @param lName
     * @param email
     * @param role
     * @param password1
     * @param password2
     * @return true if all fields are filled, false otherwise
     */
    private boolean everyDataIsFilled(String username, String fName, String lName, String email,
                                      String role, String password1, String password2) {
        return username != null && fName != null && lName != null && email != null &&
                role != null && password1 != null && password2 != null;
    }
}
