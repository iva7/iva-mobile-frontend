package com.example.iva.ui.loginAndRegistration;

import androidx.annotation.Nullable;

/**
 * Data validation state of the registration form.
 */
public class RegistrationFormState {
    @Nullable
    private Integer emailError;
    @Nullable
    private Integer passwordError;

    @Nullable
    private Integer emptyFieldError;
    private boolean isDataValid;

    RegistrationFormState(@Nullable Integer emailError, @Nullable Integer passwordError,
                          @Nullable Integer emptyFieldError) {
        this.emailError = emailError;
        this.emptyFieldError = emptyFieldError;
        this.passwordError = passwordError;
        this.isDataValid = false;
    }

    RegistrationFormState(boolean isDataValid) {
        this.emailError = null;
        this.emptyFieldError = null;
        this.passwordError = null;
        this.isDataValid = isDataValid;
    }

    @Nullable
    Integer getEmailError() {
        return emailError;
    }

    @Nullable
    Integer getPasswordError() {
        return passwordError;
    }

    @Nullable
    Integer getEmptyFieldError() {
        return emptyFieldError;
    }

    boolean isDataValid() {
        return isDataValid;
    }
}
