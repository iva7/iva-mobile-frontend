package com.example.iva.ui.creator;

import androidx.annotation.Nullable;

class InternshipFormState {

    @Nullable
    private Integer invalidDateError;
    @Nullable
    private Integer emptyFieldError;
    private boolean isDataValid;

    public InternshipFormState(boolean isDataValid) {
        this.invalidDateError = null;
        this.emptyFieldError = null;
        this.isDataValid = isDataValid;
    }

    public InternshipFormState(@Nullable Integer invalidDateError, @Nullable Integer emptyFieldError) {
        this.invalidDateError = invalidDateError;
        this.emptyFieldError = emptyFieldError;
        this.isDataValid = false;
    }


    @Nullable
    public Integer getInvalidDateError() {
        return invalidDateError;
    }

    @Nullable
    public Integer getEmptyFieldError() {
        return emptyFieldError;
    }

    public boolean isDataValid() {
        return isDataValid;
    }

    public void setInvalidDateError(@Nullable Integer invalidDateError) {
        this.invalidDateError = invalidDateError;
    }

    public void setEmptyFieldError(@Nullable Integer emptyFieldError) {
        this.emptyFieldError = emptyFieldError;
    }
}
