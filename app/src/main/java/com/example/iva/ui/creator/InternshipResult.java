package com.example.iva.ui.creator;

import androidx.annotation.Nullable;

class InternshipResult {
    @Nullable
    private boolean success;
    @Nullable
    private Integer error;
    @Nullable
    private String errorMessage;

    InternshipResult(@Nullable Integer error, @Nullable String ErrorMessage) {

        this.error = error;
        this.errorMessage = ErrorMessage;
    }

    InternshipResult(@Nullable boolean success) {
        this.success = success;
    }

    @Nullable
    Boolean getSuccess() {
        return success;
    }

    @Nullable
    Integer getError() {
        return error;
    }

    @Nullable
    public String getErrorMessage() {
        return errorMessage;
    }
}
