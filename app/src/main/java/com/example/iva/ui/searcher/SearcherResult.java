package com.example.iva.ui.searcher;

import androidx.annotation.Nullable;

class SearcherResult {
    @Nullable
    private boolean success;
    @Nullable
    private Integer error;
    @Nullable
    private String errorMessage;

    SearcherResult(@Nullable Integer error, @Nullable String ErrorMessage) {

        this.error = error;
        this.errorMessage = ErrorMessage;
    }

    SearcherResult(@Nullable boolean success) {
        this.success = success;
    }

    @Nullable
    Boolean getSuccess() {
        return success;
    }

    @Nullable
    Integer getError() {
        return error;
    }

    @Nullable
    public String getErrorMessage() {
        return errorMessage;
    }
}
