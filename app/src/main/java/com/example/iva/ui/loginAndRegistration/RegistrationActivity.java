package com.example.iva.ui.loginAndRegistration;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.iva.R;
import com.example.iva.ui.utils.Constants;

/**
 * Activity to register a user
 **/
public class RegistrationActivity extends AppCompatActivity {

    private RegistrationViewModel registrationViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        configureForm();

    }

    /**
     * Sets observers on the form fields, on the registration button and send request on click on it.
     */
    private void configureForm() {
        final EditText usernameEditText = findViewById(R.id.usernameEditText);
        final EditText fNameEditText = findViewById(R.id.firstNameEditText);
        final EditText lNameEditText = findViewById(R.id.lastNameEditText);
        final EditText emailEditText = findViewById(R.id.emailEditText);
        final EditText password1EditText = findViewById(R.id.passwordEditText);
        final EditText password2EditText = findViewById(R.id.verifyPasswordEditText);
        final RadioGroup roleGroup = findViewById(R.id.radiogroup);
        final ProgressBar loadingProgressBar = findViewById(R.id.progressBar);


        for (int i = 0; i < Constants.USERTYPES.length; i++) {
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(Constants.USERTYPES[i]);
            radioButton.setId(100+i);
            radioButton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            roleGroup.addView(radioButton);
        }

        registrationViewModel = ViewModelProviders.of(this, new ViewModelFactory())
                .get(RegistrationViewModel.class);
        final Button registrationButton = (Button) findViewById(R.id.registrationButton);

        registrationViewModel.getRegistrationFormState().observe(this, new Observer<RegistrationFormState>() {
            @Override
            public void onChanged(@Nullable RegistrationFormState registrationFormState) {
                if (registrationFormState == null) {
                    return;
                }
                registrationButton.setEnabled(registrationFormState.isDataValid());
                if (registrationFormState.getEmptyFieldError() != null) {
                    // Toast.makeText(getApplicationContext(),R.string.empty_fields, Toast.LENGTH_SHORT).show();
                }
                if (registrationFormState.getEmailError() != null) {
                    emailEditText.setError(getString(registrationFormState.getEmailError()));
                }
                if (registrationFormState.getPasswordError() != null) {
                    password2EditText.setError(getString(registrationFormState.getPasswordError()));
                }
            }
        });

        registrationViewModel.getRegistrationResult().observe(this, new Observer<RegistrationResult>() {
            @Override
            public void onChanged(@Nullable RegistrationResult registrationResult) {
                if (registrationResult == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
                if (registrationResult.getError() != null) {
                    Toast.makeText(getApplicationContext(), registrationResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (registrationResult.getSuccess() != null) {
                    Toast.makeText(getApplicationContext(), R.string.registration_succeed, Toast.LENGTH_LONG).show();
                }
                setResult(Activity.RESULT_OK);

                //Complete and destroy registration activity once successful
                finish();
            }
        });

        registrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadingProgressBar.setVisibility(View.VISIBLE);

                RadioButton radioButton = (RadioButton) findViewById(roleGroup.getCheckedRadioButtonId());
                String role = null;
                if (radioButton != null) {
                    role = radioButton.getText().toString();
                }
                registrationViewModel.registration(usernameEditText.getText().toString(),
                        fNameEditText.getText().toString(), lNameEditText.getText().toString(),
                        password1EditText.getText().toString(), emailEditText.getText().toString(),
                        role);

            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                RadioButton radioButton = (RadioButton) findViewById(roleGroup.getCheckedRadioButtonId());
                String role = null;
                if (radioButton != null) {
                    role = radioButton.getText().toString();
                }
                registrationViewModel.registrationDataChanged(usernameEditText.getText().toString(),
                        fNameEditText.getText().toString(), lNameEditText.getText().toString(),
                        emailEditText.getText().toString(), password1EditText.getText().toString(),
                        password2EditText.getText().toString(), role);
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        fNameEditText.addTextChangedListener(afterTextChangedListener);
        lNameEditText.addTextChangedListener(afterTextChangedListener);
        emailEditText.addTextChangedListener(afterTextChangedListener);
        password2EditText.addTextChangedListener(afterTextChangedListener);
        password1EditText.addTextChangedListener(afterTextChangedListener);

        roleGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                String role = null;
                if (radioButton != null) {
                    role = radioButton.getText().toString();
                }
                registrationViewModel.registrationDataChanged(usernameEditText.getText().toString(),
                        fNameEditText.getText().toString(), lNameEditText.getText().toString(),
                        emailEditText.getText().toString(), password1EditText.getText().toString(),
                        password2EditText.getText().toString(), role);
            }
        });

    }
}

