package com.example.iva.ui.loginAndRegistration;

import androidx.annotation.Nullable;

/**
 * Registration result : success or error code and message.
 */
public class RegistrationResult {
    @Nullable
    private boolean success;
    @Nullable
    private Integer error;
    @Nullable
    private String errorMessage;

    RegistrationResult(@Nullable Integer error, @Nullable String ErrorMessage) {

        this.error = error;
        this.errorMessage = ErrorMessage;
    }

    RegistrationResult(@Nullable boolean success) {
        this.success = success;
    }

    @Nullable
    Boolean getSuccess() {
        return success;
    }

    @Nullable
    Integer getError() {
        return error;
    }

    @Nullable
    public String getErrorMessage() {
        return errorMessage;
    }
}
