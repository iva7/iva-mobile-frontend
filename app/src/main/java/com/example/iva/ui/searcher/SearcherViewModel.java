package com.example.iva.ui.searcher;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.iva.data.InternshipRepository;
import com.example.iva.data.model.Internship;

import java.util.List;

public class SearcherViewModel extends ViewModel {

    private MutableLiveData<List<Internship>> mutableInternshipList = new MutableLiveData<>();

    private InternshipRepository internshipRepository;

    private MutableLiveData<SearcherResult> listInternshipsResult = new MutableLiveData<>();

    public SearcherViewModel(InternshipRepository internshipRepository) {
        this.internshipRepository = internshipRepository;
    }

    public void onGetRequestFailed(int code, String message) {
        listInternshipsResult.setValue(new SearcherResult(code,message));
    }

    public void init() {
        mutableInternshipList = this.internshipRepository.getAllInternship(this);
    }

    public MutableLiveData<List<Internship>> getAllInternships() {
        return mutableInternshipList;
    }

}