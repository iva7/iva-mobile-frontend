package com.example.iva.ui.searcher;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.iva.R;
import com.example.iva.data.model.Internship;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.ViewHolder> {

    private static final String TAG = "RecyclerViewAdapter";

    private List<Internship> internships;

    private Context context;

    public RecycleViewAdapter(List<Internship> internships, Context context) {
        this.internships = internships;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.simple_list_item, null);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    /**
     * Setting fields's values
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called");
        holder.roleField.setText(internships.get(position).getRole());
        holder.locationTextView.setText(internships.get(position).getLocation());

        String dateString = new SimpleDateFormat("yyyy/MM/dd ").format(new Date(internships.get(position).getStartDate() * 1000));
        Log.d(TAG, dateString);
        holder.dateField.setText(dateString);

    }

    @Override
    public int getItemCount() {
        return internships.size();
    }

    /**
     * ViewHolder for the RecyclerView
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView roleField;
        public TextView locationTextView;
        public TextView dateField;
        public CardView parentLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            locationTextView = itemView.findViewById(R.id.locationField);
            roleField = itemView.findViewById(R.id.roleField);
            dateField = itemView.findViewById(R.id.dateField);
            parentLayout = itemView.findViewById(R.id.linearLayout);
        }
    }
}
