package com.example.iva.ui.searcher;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.iva.R;
import com.example.iva.data.InternshipRepository;
import com.example.iva.data.model.Internship;

import java.util.ArrayList;
import java.util.List;

public class SearcherFragment extends Fragment {

    private SearcherViewModel searcherViewModel;

    private static final String TAG = "SearcherFragment";

    private List<Internship> internships = new ArrayList<>();

    private RecyclerView recyclerView;

    View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        searcherViewModel = new SearcherViewModel(InternshipRepository.getInstance());
        searcherViewModel.init();
        searcherViewModel.getAllInternships().observe(this, new Observer<List<Internship>>() {
            @Override
            public void onChanged(List<Internship> internships) {
                if (internships != null) {
                    setInternships(internships);
                }
            }
        });
        root = inflater.inflate(R.layout.fragment_searcher, container, false);
        return root;
    }

    /**
     * Listing internships in with RecyclerView
     */
    private void configureForm() {
        Log.d(TAG, "configureForm: called");
        recyclerView = root.findViewById(R.id.recyclerView);
        RecycleViewAdapter adapter = new RecycleViewAdapter(internships, getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
    }

    public List<Internship> getInternships() {
        return internships;
    }

    public void setInternships(List<Internship> internships) {
        this.internships = internships;
        configureForm();
    }
}