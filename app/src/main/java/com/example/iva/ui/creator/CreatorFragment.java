package com.example.iva.ui.creator;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.example.iva.R;
import com.example.iva.data.InternshipRepository;
import com.example.iva.ui.utils.Constants;
import com.example.iva.ui.utils.CustomDateTimePicker;

import java.util.Calendar;
import java.util.Date;

public class CreatorFragment extends Fragment {

    EditText roleEditText;
    EditText companyEditText;
    EditText locationEditText;
    EditText durationEditText;
    TextView startDate;
    TextView payedStateText;
    Switch payedSwitch;
    View salaryViewGroup;
    EditText salaryEditText;
    RadioGroup educationGroup;
    RadioButton educationRadioButton;
    EditText description;
    private CreatorViewModel creatorViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        creatorViewModel = new CreatorViewModel(InternshipRepository.getInstance());
        final View root = inflater.inflate(R.layout.fragment_creator, container, false);
        roleEditText = root.findViewById(R.id.roleEditText);
        companyEditText = root.findViewById(R.id.companyEditText);
        locationEditText = root.findViewById(R.id.locationEditText);
        durationEditText = root.findViewById(R.id.durationEditText);
        startDate = root.findViewById(R.id.startDateEditText);
        setupDatePicker(startDate);
        payedStateText = root.findViewById(R.id.payedStateText);
        payedSwitch = root.findViewById(R.id.payedSwitch);
        setupSwitchPayed();
        salaryViewGroup = root.findViewById(R.id.salaryViewGroup);
        salaryEditText = root.findViewById(R.id.salaryEditText);
        educationGroup = root.findViewById(R.id.educationRadioGroup);
        educationRadioButton = root.findViewById(educationGroup.getCheckedRadioButtonId());
        description = root.findViewById(R.id.jobDescriptionEditText);
        final ProgressBar createProgressBar = root.findViewById(R.id.progressBarCreate);
        final Button createButton = root.findViewById(R.id.createButton);

        creatorViewModel.getNewInternshipFormState().observe(this, new Observer<InternshipFormState>() {
            @Override
            public void onChanged(@Nullable InternshipFormState newInternshipFormState) {
                if (newInternshipFormState == null) {
                    return;
                }
                startDate.setError(null);
                createButton.setEnabled(newInternshipFormState.isDataValid());
                if (newInternshipFormState.getInvalidDateError() != null) {
                    startDate.setError(getString(newInternshipFormState.getInvalidDateError()));
                }
            }
        });

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createProgressBar.setVisibility(View.VISIBLE);
                if (payedSwitch.isChecked()) {
                    creatorViewModel.setPayed(true);
                    if (!salaryEditText.getText().toString().isEmpty()) {
                        creatorViewModel.setSalary(Double.parseDouble(salaryEditText.getText().toString()));
                    }
                } else {
                    creatorViewModel.setPayed(false);
                }
                educationRadioButton = root.findViewById(educationGroup.getCheckedRadioButtonId());
                @Constants.Level String education = null;
                if (educationRadioButton != null) {
                    education = educationRadioButton.getText().toString();
                }
                creatorViewModel.setEducation(education);
                creatorViewModel.setDescription(description.getText().toString());
                creatorViewModel.newInternship();
            }
        });

        creatorViewModel.getNewInternshipResult().observe(this, new Observer<InternshipResult>() {
            @Override
            public void onChanged(@Nullable InternshipResult newInternshipResult) {
                if (newInternshipResult == null) {
                    return;
                }
                createProgressBar.setVisibility(View.GONE);
                if (newInternshipResult.getError() != null) {
                    Toast.makeText(getContext(), newInternshipResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (newInternshipResult.getSuccess() != null) {
                    Toast.makeText(getContext(), R.string.new_internship_succeed, Toast.LENGTH_LONG).show();
                }
            }
        });


        roleEditText.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {/*ignore*/}
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {/*ignore*/}
                    @Override
                    public void afterTextChanged(Editable s) {
                        creatorViewModel.newInternshipRoleChanged(roleEditText.getText().toString());
                    }
                }
        );

        companyEditText.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {/*ignore*/}
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {/*ignore*/}
                    @Override
                    public void afterTextChanged(Editable s) {
                        creatorViewModel.newInternshipCompanyChanged(companyEditText.getText().toString());
                    }
                }
        );

        locationEditText.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {/*ignore*/}
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {/*ignore*/}
                    @Override
                    public void afterTextChanged(Editable s) {
                        creatorViewModel.newInternshipLocationChanged(locationEditText.getText().toString());
                    }
                }
        );

        durationEditText.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {/*ignore*/}
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {/*ignore*/}
                    @Override
                    public void afterTextChanged(Editable s) {
                        creatorViewModel.newInternshipDurationChanged(durationEditText.getText().toString());
                    }
                }
        );

        startDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {/*ignore*/}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {/*ignore*/}
            @Override
            public void afterTextChanged(Editable s) {
                creatorViewModel.newInternshipDateChanged(startDate.getText().toString());
            }
        });

        return root;
    }

    /**
     * Setting up a date picker
     *
     * @param et the edit text that will be paired with a CustomDateTimePicker
     */
    private void setupDatePicker(final TextView et) {
        final CustomDateTimePicker custom = new CustomDateTimePicker(this,
                new CustomDateTimePicker.ICustomDateTimeListener() {

                    @Override
                    public void onSet(Dialog dialog, Calendar calendarSelected,
                                      Date dateSelected, int year, String monthFullName,
                                      String monthShortName, int monthNumber, int day,
                                      String weekDayFullName, String weekDayShortName,
                                      int hour24, int hour12, int min, int sec,
                                      String AM_PM) {
                        et.setText("");
                        Calendar calendar = Calendar.getInstance();
                        if (calendar.compareTo(calendarSelected) <= 0) {
                            et.setText(year
                                    + "-" + (monthNumber + 1) + "-" + calendarSelected.get(Calendar.DAY_OF_MONTH));
                        }
                        // calendarSelected.getTimeInMillis();
                    }

                    @Override
                    public void onCancel() {

                    }
                });

        /**
         * Pass Directly current data and time to show when it pop up
         */
        custom.setDate(Calendar.getInstance());
        et.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        custom.showDialog();
                    }
                });
    }


    /**
     * Sets up the other preferences switch
     */
    private void setupSwitchPayed() {
        payedSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    payedStateText.setText(R.string.yes);
                    salaryViewGroup.setVisibility(View.VISIBLE);
                    creatorViewModel.setPayed(true);
                } else {
                    payedStateText.setText(R.string.no);
                    salaryViewGroup.setVisibility(View.GONE);
                    creatorViewModel.setPayed(false);
                }
            }
        });
    }

}