package com.example.iva.data.model;

/**
 * POJO for a login request.
 */
public class LoginRequest {
    final String username;
    final String password;

    public LoginRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
