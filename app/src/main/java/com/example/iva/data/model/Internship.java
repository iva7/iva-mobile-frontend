package com.example.iva.data.model;

import com.example.iva.ui.utils.Constants;
import com.google.gson.annotations.SerializedName;

public class Internship {
    @SerializedName("id")
    Long id;
    @SerializedName("role")
    String role;
    @SerializedName("company")
    String company;
    @SerializedName("location")
    String location;
    @SerializedName("duration")
    int duration;
    @SerializedName("startDate")
    long startDate;
    @SerializedName("payed")
    boolean payed;
    @SerializedName("salary")
    double salary;
    @Constants.Level @SerializedName("education")
    String education;
    @SerializedName("description")
    String description;

    public Internship() {
    }

    public Internship(Long id, String role, String company, String location, int duration, long startDate, boolean payed, double salary, String education, String description) {
        this.id = id;
        this.role = role;
        this.company = company;
        this.location = location;
        this.duration = duration;
        this.startDate = startDate;
        this.payed = payed;
        this.salary = salary;
        this.education = education;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public boolean isPayed() {
        return payed;
    }

    public void setPayed(boolean payed) {
        this.payed = payed;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Internship{" +
                "id=" + id +
                ", role='" + role + '\'' +
                ", company='" + company + '\'' +
                ", location='" + location + '\'' +
                ", duration=" + duration +
                ", startDate=" + startDate +
                ", payed=" + payed +
                ", salary=" + salary +
                ", education='" + education + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
