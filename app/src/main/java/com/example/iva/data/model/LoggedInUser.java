package com.example.iva.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
public class LoggedInUser {
    private static LoggedInUser instance;
    @SerializedName("id")
    private String userId;
    @SerializedName("username")
    private String displayName;
    @SerializedName("id_token")
    private String token;
    @SerializedName("email")
    private String email;
    @SerializedName("role")
    private String role;


    private String password;

    private LoggedInUser() {
    }

    public static synchronized LoggedInUser getInstance() {
        if (instance == null) {
            instance = new LoggedInUser();
        }
        return instance;
    }

    public void clear() {
        instance = null;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public static void setInstance(LoggedInUser instance) {
        LoggedInUser.instance = instance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
