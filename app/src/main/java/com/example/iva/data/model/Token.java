package com.example.iva.data.model;

import com.google.gson.annotations.SerializedName;

public class Token {
    private static Token instance;
    @SerializedName("id_token")
    String tokenId;

    private Token() {
    }

    public static synchronized Token getInstance() {
        if (instance == null) {
            instance = new Token();
        }
        return instance;
    }

    public void clear() {
        instance = null;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = "Bearer " + tokenId;
    }

}
