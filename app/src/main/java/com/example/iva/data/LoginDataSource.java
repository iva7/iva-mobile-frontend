package com.example.iva.data;

import android.util.Log;

import com.example.iva.api.LoginAPI;
import com.example.iva.data.model.LoggedInUser;
import com.example.iva.data.model.LoginRequest;
import com.example.iva.data.model.Result;
import com.example.iva.data.model.Token;
import com.example.iva.service.RetrofitInstance;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;

public class LoginDataSource {
    private String TAG = "LoginDataSource";

    public void login(String pUsername, String pPassword) {
        final String username = pUsername;
        final String password = pPassword;
        final LoginRepository loginRepo = LoginRepository.getInstance(this);


        /** Create handle for the RetrofitInstance interface*/
        LoginAPI service = RetrofitInstance.getRetrofitInstance().create(LoginAPI.class);

        /** Call the method with parameter in the interface to get the notice data*/
        Call<Token> call = service.loginUser(new LoginRequest(username, password));

        /**Log the URL called*/
        Log.wtf("URL Called", call.request().url() + "");

        call.enqueue(new Callback<Token>() {

            @Override
            public void onResponse(Call<Token> call, retrofit2.Response<Token> response) {
                Log.i(TAG, "Response :" + response.toString());
                if (response.code() != 200) {
                    loginRepo.getLoginResult(new Result.Error(new IOException("Error logging in.")));
                } else {
                    loginSucceed(response.body().getTokenId(), username, loginRepo);
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                Log.i(TAG, "Response : Error logging in.");
                loginRepo.getLoginResult(new Result.Error(new IOException("Error logging in.")));
            }
        });
    }

    /**
     * Creates a new user, stores his data and token and sends the user to the repository.
     *
     * @param token
     * @param username
     * @param loginRepo
     */
    private void loginSucceed(String token, String username, LoginRepository loginRepo) {
        Token.getInstance().setTokenId(token);
        LoggedInUser user = LoggedInUser.getInstance();
        user.setUserId(java.util.UUID.randomUUID().toString());
        user.setDisplayName(username);
        user.setToken(Token.getInstance().getTokenId());

        loginRepo.getLoginResult(new Result.Success<>(user));
    }

    /**
     * Clear the user instance and his token.
     */
    public void logout() {
        Token.getInstance().clear();
        LoggedInUser.getInstance().clear();
    }
}
