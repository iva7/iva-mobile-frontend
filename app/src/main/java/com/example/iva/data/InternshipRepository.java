package com.example.iva.data;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.iva.api.InternshipAPI;
import com.example.iva.data.model.Error;
import com.example.iva.data.model.Internship;
import com.example.iva.service.RetrofitInstance;
import com.example.iva.ui.creator.CreatorViewModel;
import com.example.iva.ui.searcher.SearcherViewModel;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InternshipRepository {
    private static volatile InternshipRepository instance;
    private String TAG = this.getClass().getSimpleName(); //logging title mark

    /**
     * Private constructor : singleton access
     */
    private InternshipRepository() {
    }

    public static InternshipRepository getInstance() {
        if (instance == null) {
            instance = new InternshipRepository();
        }
        return instance;
    }

    public void newInternship(Internship internship, CreatorViewModel pInternshipViewModel) {

        final CreatorViewModel internshipViewModel = pInternshipViewModel;

        /** Create handle for the RetrofitInstance interface*/
        InternshipAPI service = RetrofitInstance.getRetrofitInstance().create(InternshipAPI.class);

        Log.i(TAG,internship.toString());

        /** Call the method with parameter in the interface to get the notice data*/
        Call<Void> call = service.newInternship(internship);


        /**Log the URL called*/
        Log.wtf("URL Called", call.request().url() + "");

        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i(TAG, "Response :" + response.code() + response + response.body());

                if (response.code() != 200) {
                    try {
                        Error myError = (Error) RetrofitInstance.getRetrofitInstance().responseBodyConverter(
                                Error.class, Error.class.getAnnotations())
                                .convert(response.errorBody());
                        Log.i(TAG, "Response error:" + myError.getTitle());
                        internshipViewModel.getRepoNewInternshipResult(false, response.code(), myError.getTitle());
                    } catch (IOException e) {
                        Log.e(TAG, e.getLocalizedMessage());
                    }
                } else {
                    internshipViewModel.getRepoNewInternshipResult(true, 200, null);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i(TAG, "Response : Error");
                internshipViewModel.getRepoNewInternshipResult(false, 500, "This is a server error.");
            }
        });
    }

    /**
     * API call for getting all internships from database
     * @param searcherViewModel
     * @return
     */
    public MutableLiveData<List<Internship>> getAllInternship(final SearcherViewModel searcherViewModel) {
        final MutableLiveData<List<Internship>> internships = new MutableLiveData<>();

        InternshipAPI internshipAPI = RetrofitInstance.getRetrofitInstance().create(InternshipAPI.class);
        internshipAPI.getAllInternship().enqueue(new Callback<List<Internship>>() {
            @Override
            public void onResponse(Call<List<Internship>> call, Response<List<Internship>> response) {
                if (response.isSuccessful()) {
                    Log.i(TAG, "" + response.body().toString());
                    internships.setValue(response.body());
                }
                if (response.code() != 200) {
                    try {
                        Error myError = (Error) RetrofitInstance.getRetrofitInstance().responseBodyConverter(
                                Error.class, Error.class.getAnnotations())
                                .convert(response.errorBody());
                        Log.i(TAG, "Response error:" + myError.getTitle());
                        searcherViewModel.onGetRequestFailed(response.code(), myError.getTitle());
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage());
                        internships.setValue(null);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Internship>> call, Throwable t) {
                internships.setValue(null);
                Log.e(TAG,t.getMessage());
            }
        });
        return internships;
    }
}
