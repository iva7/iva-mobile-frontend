package com.example.iva.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Data class that captures error information from api call responds
 */
public class Error {
    @SerializedName("entityName")
    String entityName;
    @SerializedName("errorKey")
    String errorKey;
    @SerializedName("type")
    String type;
    @SerializedName("title")
    String title;
    @SerializedName("status")
    int status;
    @SerializedName("message")
    String message;
    @SerializedName("params")
    String params;

    public Error(String entityName, String errorKey, String type, String title, int status, String message, String params) {
        this.entityName = entityName;
        this.errorKey = errorKey;
        this.type = type;
        this.title = title;
        this.status = status;
        this.message = message;
        this.params = params;
    }

    public String getErrorKey() {
        return errorKey;
    }

    public String getTitle() {
        return title;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
