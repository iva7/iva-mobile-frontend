package com.example.iva.data;


import android.util.Log;

import com.example.iva.api.RegistrationAPI;
import com.example.iva.data.model.Error;
import com.example.iva.data.model.User;
import com.example.iva.service.RetrofitInstance;
import com.example.iva.ui.loginAndRegistration.RegistrationViewModel;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Class that requests registration.
 */
public class RegistrationRepository {
    private static volatile RegistrationRepository instance;
    private String TAG = "RegistrationRepository";

    /**
     * Private constructor : singleton access
     */
    private RegistrationRepository() {
    }

    public static RegistrationRepository getInstance() {
        if (instance == null) {
            instance = new RegistrationRepository();
        }
        return instance;
    }

    public void registration(String username, String fName, String lName, String password,
                             String email, String role, RegistrationViewModel pRegistrationViewModel) {

        final RegistrationViewModel registrationViewModel = pRegistrationViewModel;

        /** Create handle for the RetrofitInstance interface*/
        RegistrationAPI service = RetrofitInstance.getRetrofitInstance().create(RegistrationAPI.class);

        /** Call the method with parameter in the interface to get the notice data*/
        Call<Void> call = service.register(new User(username, fName, lName, password, email, role));


        /**Log the URL called*/
        Log.wtf("URL Called", call.request().url() + "");

        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i(TAG, "Response :" + response.code() + response + response.body());

                if (response.code() != 201) {
                    try {
                        Error myError = (Error) RetrofitInstance.getRetrofitInstance().responseBodyConverter(
                                Error.class, Error.class.getAnnotations())
                                .convert(response.errorBody());
                        Log.i(TAG, "Response error:" + myError.getTitle());
                        registrationViewModel.getRepoRegistrationResult(false, response.code(), myError.getTitle());
                    } catch (IOException e) {
                        e.printStackTrace();
                        registrationViewModel.getRepoRegistrationResult(false, response.code(), "Server error");
                    }
                } else {
                    registrationViewModel.getRepoRegistrationResult(true, 201, null);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i(TAG, "Response : Error");
                registrationViewModel.getRepoRegistrationResult(false, 500, "This is a server error.");
            }
        });
    }
}
