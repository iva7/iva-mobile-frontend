package com.example.iva.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * A base entity for the common fields
 */
public abstract class BaseEntity {
    @SerializedName("id")
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
